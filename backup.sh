#!/bin/bash

fakeTrue=1
fakeFalse=0

computer=$HOSTNAME
localBackup= #[[TODO replace with $fakeTrue or $fakeFalse]]
backupHost="[[TODO replace with .ssh/config name for backup server]]" # Change this based on ~/.ssh/config
rootBackupDir="[[TODO replace with path to server's backup location]]"

direction=$1
path=$2

skipConfirmYes="-y"
skipConfirmNo="-n"
if [ $# = 4 ]; then
	skipConfirm=$4
	file=$3
else
	skipConfirm=$3
	file=""
fi

skipConfirm=${skipConfirm:=$skipConfirmNo}

pullOption="pull"
pushOption="push"

sshSwitch="-e ssh"
pullSwitches="-avr"
pushSwitches="-avr"

if [ $localBackup = $fakeFalse ]; then
	pullSwitches="$pullSwitches $sshSwitch"
	pushSwitches="$pushSwitches $sshSwitch"
fi

localPath="$path/$file"
backupPath="$rootBackupDir/$computer$path"
absoluteBackupLocation="$backupHost:$backupPath"

confirmedPush="Pushing...."
confirmedPull="Pulling...."
cancelled="Cancelled."
autoConfirm="Skipped Confirmation."

function confirmBackup {

	if [ $1 = $fakeTrue ]; then
		echo $autoConfirm
		return $fakeTrue
	fi

	yesOption="y"
	noOption="n"
	confirm="Continue? [$yesOption|$noOption]"
	invalidInput="Must either enter $yesOption or $noOption"

	echo $confirm
	read response

	if [ $response = $yesOption ]; then
		return $fakeTrue
	elif [ $response = $noOption ]; then
		return $fakeFalse
	else
		echo $invalidInput
	fi
}

echo $skipConfirm

###################
#                 #
# ~~~ PULLING ~~~ #
#                 #
###################

if [ $direction = $pullOption ]; then

	echo "Pulling from backup"
	echo "Backup server: $backupHost"
	echo "Backup directory: $backupPath"
	echo "Switches: $pullSwitches"

	if [ $skipConfirm = $skipConfirmYes ]; then
		confirmBackup $fakeTrue
		continue=$?
	else
		confirmBackup $fakeFalse
		continue=$?
	fi

	if [ $continue = $fakeTrue ]; then
		echo $confirmedPull
		if [ $localBackup = $fakeTrue ]; then
			echo rsyc $pullSwitches $backupPath $localPath
		else	
			rsync $pullSwitches $backupPath $localPath
		fi	
	else 
		echo $cancelled
	fi

###################
#                 #
# ~~~ PUSHING ~~~ #
#                 #
###################

elif [ $direction = $pushOption ]; then

	echo "Pushing to backup"
	echo "Backup server: $backupHost"
	echo "Backup directory: $backupPath"
	echo "Switches: $pushSwitches"

	if [ $skipConfirm = $skipConfirmYes ]; then
		confirmBackup $fakeTrue
		continue=$?
	else
		confirmBackup $fakeFalse
		continue=$?
	fi

	if [ $continue = $fakeTrue ]; then
		echo $confirmedPush
		if [ $localBackup = $fakeTrue ]; then	
			mkdir -p $backupPath
			rsync $pushSwitches $localPath $backupPath
		else
			rsync $pushSwitches $localPath $absoluteBackupLocation
			ssh $backupHost "mkdir -p $backupPath"
		fi	
	else
		echo $cancelled
	fi

else

	echo "First option must be either $pushOption or $pullOption"

fi


