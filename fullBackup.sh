#!/bin/bash

dirArr=(
	'folder/subfolder'
	'folder/subfolder file'
)

for f in "${dirArr[@]}"
do
	/bin/bash ~/scripts/backup.sh push $f -y
done
